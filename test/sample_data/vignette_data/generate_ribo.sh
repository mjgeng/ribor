
REFNAME=appris_human_24_01_2019
TLEN=/data/projects/ribo/RIBOflow/sample_data/appris_human_24_01_2019_selected.lengths.tsv
ANNOTATION=/data/projects/ribo/RIBOflow/sample_data/appris_human_24_01_2019_actual_regions.bed
RIBOMETA=./metadata/ing_hek293.yaml


EXPMETA=metadata/GSM1606107.yml
#INPUTREADS=./bed/GSM1606107.merged.post_dedup.bed
INPUTREADS=./bed/GSM1606107.random.sample.bed
RNASEQ=rnaseq/GSM1606099.bam

time ribog create --name GSM1606107 --alignmentfile ${INPUTREADS} --reference ${REFNAME} \
             --lengths ${TLEN} --annotation ${ANNOTATION} --metageneradius 35 \
             --leftspan 35 --rightspan 15 \
	     --lengthmin 28 --lengthmax 32 \
             --expmeta ${EXPMETA} --ribometa ${RIBOMETA} \
             -n 8  \
             GSM1606107.ribo

ribog rnaseq set -n GSM1606107 -a ${RNASEQ} -f bam GSM1606107.ribo

EXPMETA=metadata/GSM1606108.yml
#INPUTREADS=./bed/GSM1606108.merged.post_dedup.bed
INPUTREADS=./bed/GSM1606108.random.sample.bed
RNASEQ=rnaseq/GSM1606100.bam


time ribog create --name GSM1606108 --alignmentfile ${INPUTREADS} --reference ${REFNAME} \
             --lengths ${TLEN} --annotation ${ANNOTATION} --metageneradius 35 \
             --leftspan 35 --rightspan 15 \
	     --lengthmin 28 --lengthmax 32 \
             --expmeta ${EXPMETA} --ribometa ${RIBOMETA} \
             -n 8  \
             GSM1606108.ribo

ribog rnaseq set -n GSM1606108 -a ${RNASEQ} -f bam GSM1606108.ribo


ribog merge HEK293_ing.ribo GSM1606107.ribo GSM1606108.ribo
