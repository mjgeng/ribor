% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/region_count_functions.R
\name{get_length_distribution}
\alias{get_length_distribution}
\title{Retrieves the length distribution of a given region}
\usage{
get_length_distribution(ribo.object, region, range.lower, range.upper,
  experiments = get_experiments(ribo.object), total = TRUE)
}
\arguments{
\item{ribo.object}{A 'ribo' object}

\item{region}{Specific region of interest}

\item{range.lower}{Lower bound of the read length}

\item{range.upper}{Upper bound of the read length}

\item{experiments}{List of experiment names}

\item{total}{Include a column with the total reads, required for plotting}
}
\value{
A data table of the counts at each read length
}
\description{
The function {\code{\link{get_length_distribution}}} retrieves the raw or normalized 
counts at each read length from 'range.lower' to 'range.upper'.
}
\details{
This function is a wrapper function of {\code{\link{get_region_counts}}}, and the 
returned data table is valid input for {\code{\link{plot_length_distribution}}}.
}
\examples{
#generate the ribo object
file.path <- system.file("extdata", "sample.ribo", package = "ribor")
sample <- ribo(file.path)

#specify the experiments of interest 
experiments <- c("Hela_1", "Hela_2", "WT_1")

#gets the normalized length distribution from read length 2 to 5 
length.dist <- get_length_distribution(ribo.object = sample,
                                       region = "CDS",
                                       range.lower = 2,
                                       range.upper = 5)

}
\seealso{
{\code{\link{plot_length_distribution}}} to plot the output of this function
}
