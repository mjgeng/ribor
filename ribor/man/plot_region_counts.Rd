% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/region_count_functions.R
\name{plot_region_counts}
\alias{plot_region_counts}
\title{Plots the region counts of UTR5, CDS, and UTR3}
\usage{
plot_region_counts(x, range.lower, range.upper, experiments,
  title = "Region Counts")
}
\arguments{
\item{x}{A 'ribo' object or a data table generated from \code{\link{get_region_counts}}}

\item{range.lower}{a lower bounds for a read length range}

\item{range.upper}{an upper bounds for a read length range}

\item{experiments}{a list of experiment names}

\item{title}{a title for the generated plot}
}
\value{
A 'ggplot' of the region counts
}
\description{
The function \code{\link{plot_region_counts}} can take either a data.table
or a "ribo" object to generate the a stacked bar plot of proportions that
correspond to the "UTR5", "CDS", and "UTR3" regions.
}
\details{
When given a 'ribo' object, \code{\link{plot_region_counts}} calls
\code{\link{get_region_counts}} to retrieve the necessary information
for plotting. This option is in the case that a data.table of the
region count information is not required.

The user can instead provide a data.table with the same structure as the
output of the \code{\link{get_region_counts}} function where the 'transcript'
and 'length' parameters are the default values of TRUE. This also means that
the remaining parameters of the \code{\link{plot_region_counts}} function are not necessary.
The run time becomes substantially faster when \code{\link{plot_region_counts}} is given
the direct data.table to plot. Note that there is no manipulation by this function on the
data.table, making this input option more error prone.
}
\examples{
#ribo object use case
#generate the ribo object
file.path <- system.file("extdata", "sample.ribo", package = "ribor")
sample <- ribo(file.path)

#specify the regions and experiments of interest
regions <- c("UTR5", "CDS", "UTR3")
experiments <- c("Hela_1", "Hela_2", "WT_1")

plot_region_counts(sample,
                   range.lower = 2,
                   range.upper = 5,
                   experiments)

#data.table use case
#obtains the region counts at each individual read length, summed across every transcript
region.counts <- get_region_counts(sample,
                                   region = regions,
                                   range.lower = 2,
                                   range.upper = 5,
                                   tidy = TRUE,
                                   length = TRUE,
                                   transcript = TRUE)

#the params 'length' and 'transcript' must be set to true to use a data.table
plot_region_counts(region.counts)

}
\seealso{
\code{\link{get_region_counts}} to generate a data.table that can be provided as input,
\code{\link{ribo}} to create a ribo.object that can be provided as input
}
