[![Build Status](https://travis-ci.com/ribosomeprofiling/riboR_alpha.svg?branch=master)](https://travis-ci.com/ribosomeprofiling/riboR_alpha)

# riboR_alpha
R api for ribo files

This is the temporary repository that we will use to develop our R api for ribo files.

Once ready, we can move it to a permanent repository with a cleaner history.

# Installation

RiboR requires R version 3.6 or higher.

## Note For Linux Users

Install dependencies for devtools.
For Ubuntu based distributions, you can use the following command.

`sudo apt-get install libcurl4-openssl-dev libssl-dev -y`

## Install Latest Version From GitHub

1) Install devtools

`install.packages("devtools")`

2) Load the devtools package

`library("devtools")`

3) Install RiboR from github

`install_github("ribosomeprofiling/ribor_alpha/ribor")`
